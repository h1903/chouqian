<?php
/**
 * Created by PhpStorm.
 * User: huanglintian
 * Date: 2019/4/25
 * Time: 15:18
 */
$data = require 'data.php';
$isAd = isset($_GET['path'])&&!empty($_GET['path']) ? true : false;
if ($isAd) {
    header("location:{$data['back_url']}");
    die;
}

$hasFile = isset($_GET['file'])&&!empty($_GET['file']) ? true : false;

if ($hasFile) {
    header("location:http://{$data['d1']}/first.php?t=".time());
} else {
    header("location:http://www.google.com");
}