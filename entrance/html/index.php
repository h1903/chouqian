<?php
/**
 * Created by PhpStorm.
 * User: huanglintian
 * Date: 2019/5/31
 * Time: 11:58
 */
$data = require '../data.php';
if (isset($_GET['form']) && !empty($_GET['form'])) {
    $form = trim($_GET['form']);
    header("location:http://{$data['d1']}/form/{$form}?t=".time());
}