<?php
/**
 * Created by PhpStorm.
 * User: huanglintian
 * Date: 2018/12/25
 * Time: 15:23
 */
$rote = isset($_GET['s']) ? trim($_GET['s']) : '';
$data = include 'data.php';
include_once 'include.php';
if (preg_match('/\/;(\d*?);\//', $rote, $matches))  {
    include 'show2.php';
} else if (preg_match('/\/(\d*?)\//', $rote, $matches))  {
    include 'show.php';
} else if (preg_match('/;(jump)/', $rote, $matches)) {
    include 'jump.php';
} else if (preg_match('/;(raorao)/', $rote, $matches)) {
    include 'rao.php';
} else {
    include 'cover.php';
}
