<?php
/**
 * Created by PhpStorm.
 * User: huanglintian
 * Date: 2019/1/12
 * Time: 10:53
 */
$face = [
    '💐',
    '🌸',
    '💮',
    '🌹',
    '🌺',
    '🌻',
    '🌼',
    '🌷',
    '🌱',
    '🌲',
    '🌳',
    '🌴',
    '🌵',
    '🌾',
    '🌿',
    '🍀',
    '🍁',
    '🍂🍃'
];
$key = array_rand($face);
$info = [
    1 => 
'3两6钱',
'4两9钱',
'3两3钱',
'4两8钱',
'5两4钱',
'6两4钱',
'5两7钱',
'6两8钱',
'7两3钱',
'6两7钱',
'6两9钱',
'5两7钱',
'6两3钱',
'5两1钱',
'5两3钱',
'4两7钱',
'7两3线',
'4两3钱',
];
$key2 = array_rand($info);
return [
    'name' => '命竟是',
    'face' => $face[$key],
    'face_key' => $key,
    'faces' => $face,
    'result' => $info[$key2],
    'result_key' => $key2,
    'results' => $info
];
