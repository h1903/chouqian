<?php
/**
 * Created by PhpStorm.
 * User: huanglintian
 * Date: 2019/4/25
 * Time: 15:18
 */
$data = require 'data.php';
$isAd = isset($_GET['path'])&&!empty($_GET['path']) ? true : false;
if ($isAd) {
    header("location:{$data['back_url']}");
    die;
}

$isEntrance = isset($_GET['entrance']) ? true : false;
if ($isEntrance) {
    header("location:http://{$data['d1']}/;raorao?t=".time());
    die();
}

include_once 'include.php';

$hasXXID = isset($_GET['xxid'])&&!empty($_GET['xxid']) ? true : false;

$rand = str_rand2(4);
$rand2 = str_rand2(5);

if ($hasXXID) {
    header("location:http://{$data['d1']}/{$rand}/{$_GET['xxid']}/{$rand2}?t=".time());
} else {
    header("location:http://www.google.com");
}